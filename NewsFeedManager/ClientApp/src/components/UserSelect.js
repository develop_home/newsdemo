import React, { Component } from 'react';
import { userStoreTypes, userStore } from '../stores';
import './UserSelect.css';

export class UserSelect extends Component {
	displayName = UserSelect.name

	constructor(props) {
		super(props);

		this.store = userStore;
		this.state = this.store.getState();
		this.store.subscribe(() => {
			this.setState(this.store.getState()); // eslint-disable-line react/no-set-state
		});

		this.handleChange = this.handleChange.bind(this);
	}

	handleChange(e) {
		if (this.state.users.length > 0){

			this.store.dispatch({
				type: userStoreTypes.SET_CURRENT_USER,
				data: this.state.users.filter(u => u.id === parseInt(e.target.value, 10)).shift() // takes the first user with the id
			});

		}
	}

	componentDidMount(){
		fetch('/api/users/ListUsers')
		.then(response => response.json())
		.then(res => {
			this.store.dispatch({
				type: userStoreTypes.SET_USERS,
				data: res,
			});

		})
		.catch(() => alert('An error ocurrs loading the users.'));


	}

	render() {
		return (
			<select className="form-control" value={this.state.currentUser.id} onChange={this.handleChange}>
				{this.state.users.map(user => <option key={user.id} value={user.id}>{ user.firstName + ', ' + user.lastName }</option>)}
			</select>
		);
	}
}
