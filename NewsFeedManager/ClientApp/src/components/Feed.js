import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';

export class Feed extends Component {
  displayName = Feed.name

  static renderItems(items) {
		return (
			<Grid fluid>
				{
					items.map((item, index) => (
						<Row className="panel panel-default alert-default" key={index} >
							<a href={item.link}>
								<Col md={11} >
									<h5><b>{item.title}</b></h5>
									<h6>{item.description}</h6>
								</Col>
							</a>
						</Row>
					))
				}
			</Grid>
		);
	}

  render() {
  	const hasItems = this.props.items && this.props.items.length > 0;
		let contents = this.props.loading
		? <p><em>Loading items...</em></p>
		: hasItems ? 
			Feed.renderItems(this.props.items) : 
				<h4>No items to show.</h4>
		return (
			<React.Fragment>{contents}</React.Fragment>
	  );
  }
}
