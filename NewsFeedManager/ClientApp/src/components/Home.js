import React, { Component } from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import { UserSelect } from './UserSelect';
import { FeedList } from './FeedList';
import { Feed } from './Feed';
import { userStore } from '../stores';
import './Home.css'
const isValueIncluded = (title, description, search) => 
						title.toLowerCase().includes(search.toLowerCase()) ||
						description.toLowerCase().includes(search.toLowerCase())

const defaultState = () => ({
	newChannel: '',
	currentUser: null,
	currentFeed: null,
	userFeeds: [],
	feedsFilter: '',
	filteredUserFeeds: null,
	feedItems: [],
	feedsItemsFilter: '',
	filteredFeedsItems: null,
	loadingFeeds: false,
	loadingFeedItems: false,
	loadAllFeedsItems: false
});

export class Home extends Component {
	displayName = Home.name

	constructor(props) {
		super(props);

		this.userStore = userStore;
		this.state = defaultState()

		this.userStore.subscribe(() => {
			this.loadUserFeeds();
		});

		this.bindFunctions();
	}

	bindFunctions() {
		this.handleFeedSelection = this.handleFeedSelection.bind(this)
		this.handleChannelChange = this.handleChannelChange.bind(this)
		this.handleSubscribe = this.handleSubscribe.bind(this)
		this.refreshFeedItemsList = this.refreshFeedItemsList.bind(this)
		this.handleAllFeedsSelection = this.handleAllFeedsSelection.bind(this)
		this.handleChangeFeedsFilter = this.handleChangeFeedsFilter.bind(this)
		this.handleChangeFeedsItemsFilter = this.handleChangeFeedsItemsFilter.bind(this)
	}

	loadUserFeeds() {
		this.setState({ ...this.state, loadingFeeds: true });
		this.loadUserData();
	}

	loadUserData(){
		fetch(`/api/feeds/GetFeedsByUserId?userId=${this.userStore.getState().currentUser.id}`)
			.then(response => response.json())
			.then(res => {
				this.setState({
					...this.state,
					currentUser: this.userStore.getState().currentUser,
					userFeeds: res,
					feedItems: [],
					loadingFeeds: false
				}, this.refreshFeedItemsList());
			})
			.catch(() => {
				alert('An error ocurrs loading the feeds of this user.')

				this.setState({
					...this.state,
					currentUser: this.userStore.getState().currentUser,
					userFeeds: [],
					loadingFeeds: false
				});
			});
	}

	loadAllItemsForAFeed(feedId) {

		fetch(`/api/items/getItemsByFeedId?feedId=${feedId}`)
		.then(response => response.json())
		.then(res => {
			this.setState({
				...this.state,
				feedItems: res,
				loadingFeedItems: false
			});
		})
		.catch(() => alert('An error ocurrs loading the feed items.'));
	}

	loadAllItemsForAnUser(userId) {
		fetch(`/api/feeds/getAllFeedItemsByUserId?userId=${userId}`)
		.then(response => response.json())
		.then(res => {
			var allItems = res.map(feed => feed.items).reduce((a, b) => [...a, ...b]);
			this.setState({
				...this.state,
				feedItems: allItems,
				loadingFeedItems: false
			});
		})
		.catch(() => alert('An error ocurrs loading the feed items.'));

	}

	refreshFeedItemsList(){
		this.setState({ ...this.state, feedItems: [], loadingFeedItems: true });
		
		if (this.state.loadAllFeedsItems){
			if (this.state.currentUser){
				this.loadAllItemsForAnUser(this.state.currentUser.id);
			}
		}

		else if (this.state.currentFeed){
			this.loadAllItemsForAFeed(this.state.currentFeed.id)
		}

		else {
			this.setState({
				...this.state,
				feedItems: [],
				loadingFeedItems: false
			});
		}
	}

	handleFeedSelection(feed) {
		this.setState({ ...this.state, currentFeed: feed }, this.refreshFeedItemsList);
	}


	handleAllFeedsSelection(e) {
		this.setState({ ...this.state, loadAllFeedsItems: e.target.checked }, this.refreshFeedItemsList);
	}

	handleChannelChange(e) {
		this.setState({
			...this.state,
			newChannel: e.target.value
		});
	}

	handleSubscribe(e) {
		const url = '/api/users/asociateChannel';
		fetch(url, {
			method: "PUT",
			headers: {
				"Content-Type": "application/json",
				"userId": this.userStore.getState().currentUser.id
			},
			body: JSON.stringify(this.state.newChannel)
		})
			.then(response => {
					if (response.status >= 200 && response.status <= 299){
						return response;
					}
					throw new Error(response)
				})
			.then(response => response.json())
			.then(res => {
					alert('Now you are subscribed to the channel');
					this.setState({
						...this.state,
						newChannel: ''
					}, this.loadUserData());
			})
			.catch(() => {
				alert('An error occurred subscribing to the channel.')
			});
	}

	handleChangeFeedsFilter(e) {
		const filterValue = e.target.value;
		const userFeeds = this.state.userFeeds;

		this.setState({
			...this.state,
			currentFeed: null,
			feedsFilter: filterValue,
			filteredUserFeeds: filterValue ? userFeeds.filter(feed => isValueIncluded(feed.title, feed.description , filterValue)) : null
		}, this.refreshFeedItemsList);
	}

	handleChangeFeedsItemsFilter(e) {
		const filterValue = e.target.value;
		const userFeeds = this.state.feedItems;

		this.setState({
			...this.state,
			feedsItemsFilter: filterValue,
			filteredFeedsItems: filterValue ? 
									userFeeds.filter(item => isValueIncluded(item.title, item.description, filterValue)) : null
		});
	}

	render() {
		const showLoadBar = (this.state.loadingFeedItems || this.state.loadingFeeds)
		return (
			<div className="container-fluid">
				<h2> Feeds Reader </h2>
								
				<div className="panel-group">
					<div className="panel panel-default">
						<div className="panel-heading">Basic Information</div>
							<div className="panel-body">

								<Grid fluid>
									<Row>
										<Col sm={1}><label className="label label-primary">User</label></Col>
										<Col sm={2}><UserSelect /></Col>
									</Row>

									<Row>
										<Col sm={1}>
											<label className="label label-primary" >Add new channel</label>
										</Col>
										<Col sm={5}>
											<input type="text"
													placeholder="Put here the feed address that you want to add"
													className="form-control"
													value={this.state.newChannel} 
													onChange={this.handleChannelChange} />
										</Col>
										<Col sm={6} >
											<input className="btn btn-primary pull-right" 
													type="button" 
													value="Subscribe"
													disabled={!this.state.newChannel} 
													onClick={this.handleSubscribe}/>
										</Col>
									</Row>
								</Grid>

							</div>
					</div>
					{showLoadBar &&
						<div 
							className="progress-bar progress-bar-striped active" 
							role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" 
							style={{width: '100%'}}>
							Loading...
						</div>
					}

					{
						this.state.userFeeds.length > 0 &&
						<div className="panel panel-default">
							<div className="panel-heading">Suscribed feeds</div>
							<div className="panel-body">

									<Grid fluid>
										<Row>
											<Col sm={1}><label className="label label-primary">Load all feeds</label></Col>
											<Col sm={1}>
											<input type="checkbox"
														className="form-check"
														defaultChecked={this.state.loadAllFeedsItems} 
														onChange={this.handleAllFeedsSelection} /></Col>
										</Row>
		
										<Row>
											<Col sm={1}>
												<label className="label label-primary" >Filter feeds: </label>
											</Col>
											<Col sm={2}>
												<input type="text" 
														className="form-control"
														value={this.state.feedsFilter} 
														onChange={this.handleChangeFeedsFilter} />
											</Col>
										</Row>
		
										<Row>
											<Col sm={12}>
												<FeedList
													currentFeed={this.state.currentFeed}
													feeds={this.state.filteredUserFeeds || this.state.userFeeds}
													loading={this.state.loadingFeeds}
													handleFeedSelection={this.handleFeedSelection}
												/>
											</Col>
										</Row>
									</Grid>

							</div>
						</div>
					}


					{
						this.state.feedItems.length > 0  &&
						<div className="panel panel-default">
							<div className="panel-heading">Suscribed feeds items</div>
							<div className="panel-body">

								<Grid fluid>
									<Row>
										<Col sm={1}>
											<label className="label label-primary " >Filter items: </label>
										</Col>
										<Col sm={2}>
											<input type="text"
													className="form-control"
													value={this.state.feedsItemsFilter} 
													onChange={this.handleChangeFeedsItemsFilter} />
										</Col>
									</Row>
									<Row>
										<Col sm={12}>
											<Feed
												items={ this.state.filteredFeedsItems || this.state.feedItems }
												loading={ this.state.loadingFeedItems }/>
										</Col>
									</Row>
								</Grid>

							</div>
						</div>
					}
					
					<h6 className="info">
						{!this.state.currentUser &&
							<b>choose an user to see the feed</b>
						}

						{this.state.currentUser &&
							<b>Currently viewing feeds for the user: {this.state.currentUser.username}</b>
						}
						
					</h6>
				</div>
			</div>
			);
		}
	}
