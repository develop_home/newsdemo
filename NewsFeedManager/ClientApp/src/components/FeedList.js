import React, { Component } from 'react';
import { Grid, Col, Row } from 'react-bootstrap';

export class FeedList extends Component {
	displayName = FeedList.name
	
	static renderFeeds(feeds, handleFeedSelection, currentFeedId) {
		return (
			<Grid fluid>
				{
					feeds.map(feed => (
						<Row 
							onClick={() => handleFeedSelection({...feed})} 
							className={`panel panel-default ${currentFeedId===feed.id ? 'alert-info' : 'alert-default'}`} key={`${feed.id}`} >
							<Col md={11} >
								<h5><b>{feed.title}</b></h5>
								<h6>{feed.description}</h6>
							</Col>
							<label 
								className="label label-primary pull-right" 
								onClick={() => handleFeedSelection({...feed})}>Read Items</label>
						</Row>
					))
				}
			</Grid>
		);
	  }

	render() {
		const hasFeeds = this.props.feeds && this.props.feeds.length > 0;
		const currentFeedId = this.props.currentFeed ? this.props.currentFeed.id : null;
		let contents = this.props.loading
		? <p><em>Loading feeds...</em></p>
		: hasFeeds ? 
				FeedList.renderFeeds(this.props.feeds, this.props.handleFeedSelection, currentFeedId) : 
				<h4>No items to show.</h4>
		return (
			<React.Fragment>{contents}</React.Fragment>
		);
	}
}
