import React, { Component } from 'react';
import { Grid, Row } from 'react-bootstrap';

export class Layout extends Component {
  displayName = Layout.name

  render() {
    return (
      <Grid fluid>
        <Row>
            {this.props.children}
        </Row>
      </Grid>
    );
  }
}
