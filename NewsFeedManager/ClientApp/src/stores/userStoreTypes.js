
export const SET_CURRENT_USER = 'SET_CURRENT_USER';
export const SET_USERS = 'SET_USERS';

export default {
	SET_CURRENT_USER,
	SET_USERS
};
