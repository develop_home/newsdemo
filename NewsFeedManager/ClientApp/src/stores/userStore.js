import { createStore } from 'redux';
import * as UserStoreTypes from './userStoreTypes';

const defaultState = {
	currentUser: {id: 0, userName: '', firstName:'', lastName:''},
	users: []
};

function UserStore(currentState, action) {
	const newState = currentState || defaultState;

	switch (action.type) {
	case UserStoreTypes.SET_USERS:
		return Object.assign({}, newState, { users: action.data, currentUser: action.data[0] });

	case UserStoreTypes.SET_CURRENT_USER:
		return Object.assign({}, newState, { currentUser: action.data });
		
	default:
		return newState;
	}
}

export default createStore(UserStore);
