﻿using FeedsDomain.Common;
using FeedsDomain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FeedsManager.Controllers
{
    [Route("api/feeds")]
    [ApiController]
    public class FeedsController : ControllerBase
    {
        private IFeedService _feedService;

        public FeedsController(IFeedService feedService)
        {
            _feedService = feedService;
        }

        [HttpGet("getFeedsByUserId")]
        public IActionResult GetFeedsByUserId(int userId)
        {
            return Ok(_feedService.GetFeedsByUserId(userId));
        }

        [HttpGet("getAllFeedItemsByUserId")]
        public async Task<IActionResult> GetAllFeedItemsByUserIdAsync(int userId)
        {
            try
            {
                return Ok(await _feedService.GetAllFeedItemsByUserIdAsync(userId));
            }
            catch (Exception)
            {
                return StatusCode(500, new InvalidOperationException(Constants.EX_ERROR_PROCESSING_DOCUMENT));
            }
        }

        [HttpPost("processFeed")]
        public async Task<IActionResult> ProcessFeedAsync(string feedUri)
        {
            try
            {
                return Ok(await _feedService.ProcessFeedUriAsync(feedUri));
            }
            catch (Exception)
            {
                return StatusCode(500, new InvalidOperationException(Constants.EX_ERROR_PROCESSING_DOCUMENT));
            }
        }

    }
}