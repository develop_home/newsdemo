﻿using FeedsDomain.Common;
using FeedsDomain.Entities;
using FeedsDomain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FeedsManager.Controllers
{
    [Route("api/users")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        [HttpPut("asociateChannel")]
        public async Task<IActionResult> AsociateChannelAsync([FromHeader]int userId, [FromBody]string channel)
        {
            try
            {
                return Ok(await _userService.AsociateFeedsAsync(userId, channel));
            }
            catch (Exception ex)
            {
                return StatusCode(500, new InvalidOperationException(Constants.EX_ERROR_PROCESSING_DOCUMENT, ex));
            }
        }

        [HttpGet("listUsers")]
        public IActionResult ListUsers()
        {
            return Ok(_userService.ListUsers());
        }
    }
}