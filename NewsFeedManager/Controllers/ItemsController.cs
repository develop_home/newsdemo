﻿using FeedsDomain.Common;
using FeedsDomain.Services;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FeedsManager.Controllers
{
    [Route("api/items")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly IFeedService _feedService;

        public ItemsController(
            IFeedService feedService)
        {
            _feedService = feedService;
        }

        [HttpGet("getItemsByFeedId")]
        public async Task<IActionResult> GetItemsByFeedId(int feedId)
        {
            try
            {
                var feed = _feedService.FindFeedById(feedId);
                var resultFeed = await _feedService.ProcessFeedUriAsync(feed.Link);

                return Ok(resultFeed.Items);

            }
            catch (Exception)
            {
                return StatusCode(500, new InvalidOperationException(Constants.EX_ERROR_PROCESSING_DOCUMENT));
            }
        }
    }
}