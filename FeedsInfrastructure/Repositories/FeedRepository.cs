﻿using FeedsDomain.Common;
using FeedsDomain.Entities;
using FeedsInfrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FeedsInfrastructure.Repositories
{
    public class FeedRepository : IFeedRepository
    {
        protected readonly FeedContext _context;

        public FeedRepository(FeedContext context)
        {
            _context = context ?? throw new ApplicationException(Constants.EX_NULL_VALUE_INJECTED);
        }

        public Feed FindFeedById(int feedId)
        {
            return _context.Feeds.Find(feedId);
        }

        public IEnumerable<Feed> GetFeedsByUserId(int userId)
        {
            return _context.Feeds.Where(f => f.User.Id == userId);
        }

        public void Save(Feed entityToSave)
        {
            // when the entity exists will be updated, otherwise it is added
            if (_context.Feeds.Any(x => x.Id == entityToSave.Id))
            {
                _context.Entry(entityToSave).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
            }
            else
            {
                _context.Feeds.Add(entityToSave);
            }

            _context.SaveChanges();
        }

        public void SaveBatch(IEnumerable<Feed> entitiesToSave)
        {
            entitiesToSave.ToList().ForEach(feed => Save(feed));
        }
    }
}
