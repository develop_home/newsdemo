﻿using FeedsDomain.Common;
using FeedsDomain.Entities;
using FeedsInfrastructure.Context;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FeedsInfrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        protected readonly FeedContext _context;
        protected readonly IFeedRepository _feedRepository;

        public UserRepository(FeedContext context, IFeedRepository feedRepository)
        {
            _context = context ?? throw new ApplicationException(Constants.EX_NULL_VALUE_INJECTED);
            _feedRepository = feedRepository ?? throw new ApplicationException(Constants.EX_NULL_VALUE_INJECTED);

            this._feedRepository = feedRepository;
        }

        public User FindUser(string userName)
        {
            return _context.Users.Where(u => u.Username == userName).FirstOrDefault();
        }

        public User FindUserById(int userId)
        {
            return _context.Users.Where(u => u.Id == userId).FirstOrDefault();
        }

        public List<User> ListUsers()
        {
            return _context.Users.ToList();
        }

        public void Save(User entityToSave)
        {
            using (var dbTransaction = _context.Database.BeginTransaction())
            {
                try
                {
                    // when the entity exists will be updated, otherwise it is added
                    if (_context.Users.Any(x => x.Id == entityToSave.Id))
                    {
                        _context.Entry(entityToSave).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    }
                    else
                    {
                        _context.Users.Add(entityToSave);
                    }
                    _context.SaveChanges();

                    _feedRepository.SaveBatch(entityToSave.Feeds());

                    dbTransaction.Commit();
                }
                catch (Exception)
                {
                    dbTransaction.Rollback();
                }
            }
        }

        public void SaveBatch(IEnumerable<User> entitiesToSave)
        {
            entitiesToSave.ToList().ForEach(user => Save(user));
        }
    }
}
