﻿using FeedsDomain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FeedsInfrastructure.Mappers
{
    public class FeedMap : IEntityTypeConfiguration<Feed>
    {
        public void Configure(EntityTypeBuilder<Feed> builder)
        {
            builder.ToTable("Feeds");
            builder.HasKey(m => m.Id);
            builder
                .Property(m => m.Id)
                .HasColumnName("FeedId")
                .ValueGeneratedOnAdd();
            builder.Property(m => m.Link).IsRequired();
            builder.Property(m => m.Title).IsRequired().HasMaxLength(1000);
            builder.Property(m => m.Description).IsRequired();

            builder
                .HasOne<User>("User")
                .WithMany()
                .IsRequired(false)
                .HasForeignKey("UserId");
        }
    }
}
