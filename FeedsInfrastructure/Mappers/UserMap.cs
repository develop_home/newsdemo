﻿using FeedsDomain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FeedsInfrastructure.Mappers
{
    public class UserMap : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");
            builder.HasKey(m => m.Id);
            builder.HasAlternateKey(m => m.Username);                                 
            builder
                .Property(m => m.Id)
                .HasColumnName("UserId")
                .ValueGeneratedOnAdd();

            builder.Property(m => m.Username).IsRequired().HasMaxLength(64);
            builder.Property(m => m.FirstName).IsRequired().HasMaxLength(64);
            builder.Property(m => m.LastName).IsRequired().HasMaxLength(64);
        }
    }
}
