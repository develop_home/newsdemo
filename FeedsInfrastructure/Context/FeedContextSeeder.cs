﻿using FeedsDomain.Entities;
using System;
using System.Collections.Generic;

namespace FeedsInfrastructure.Context
{
    public static class FeedContextSeeder
    {
        private static List<Feed> GetUserFeeds(User user) {

            var feed1 = new Feed("https://www.nytimes.com/2019/02/11/us/politics/ilhan-omar-anti-semitism.html", "NYT > Home Page", "http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml", "", user);
            var feed2 = new Feed(
               "https://www.foxnews.com/science/found-fastest-orbiting-asteroid-zips-around-sun-in-just-165-days",
               "Fastest-orbiting asteroid zips around Sun in just 165 days",
               "http://feeds.foxnews.com/foxnews/scitech",
               "Astronomers have just found an asteroid that zips around the sun every 165 Earth days", 
               user);
            var feed3 = new Feed(
                "https://abcnews.go.com/US/start-here-border-negotiations-snag-klobuchar-announces-2020-ami/story?id=60937745",
                "ABC News: Top Stories",
                "http://abcnews.go.com/abcnews/topstories",
                "It's Monday, Feb. 11, 2019. Here's what you need to start your day.",
                user);

            return new List<Feed> { feed1, feed2, feed3 };
        }

        public static void Seed(FeedContext context)
        {
            if (context.Database.EnsureCreated())
            {
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var user1 = new User("jpapa", "Jhon", "Papa");
                var user2 = new User("arodriguez", "Antonio", "Rodriguez");
                var user3 = new User("testUser", "Test", "User");

                context.Users.Add(user1);
                context.Users.Add(user2);
                context.Users.Add(user3);

                context.SaveChanges();

                var user2Feeds = GetUserFeeds(user2);
                var user3Feed = GetUserFeeds(user3)[2];

                context.Feeds.Add(user2Feeds[0]);
                context.Feeds.Add(user2Feeds[1]);
                context.Feeds.Add(user2Feeds[2]);
                context.Feeds.Add(user3Feed);

                user3.AssociateFeeds(new List<Feed>{ user3Feed });
                user2.AssociateFeeds(user2Feeds);

                context.SaveChanges();
            }
        }
    }
}
