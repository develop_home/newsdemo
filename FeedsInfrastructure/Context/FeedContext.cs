﻿using FeedsDomain.Entities;
using FeedsInfrastructure.Mappers;
using Microsoft.EntityFrameworkCore;

namespace FeedsInfrastructure.Context
{
    public class FeedContext : DbContext
    {
        public FeedContext()
        {
        }

        public FeedContext(DbContextOptions<FeedContext> options) : base(options)
        {
        }

        public DbSet<User> Users { get; set; }

        public DbSet<Feed> Feeds { get; set; }

        public DbSet<Item> Items { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new UserMap());
            modelBuilder.ApplyConfiguration(new FeedMap());
        }

    }
}
