﻿using FeedsDomain.Common;
using FeedsDomain.Entities;
using FeedsDomain.Exceptions;
using FeedsDomain.Services;
using FeedsInfrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Security.Authentication;
using System.Threading.Tasks;

namespace FeedsApplication.Services
{
    public class UserService : IUserService
    {
        private IUserRepository _userRepository;
        private IFeedService _feedService;

        public UserService(
                IFeedService feedService,
                IUserRepository userRepository)
        {
            userRepository = userRepository ?? throw new ApplicationException(Constants.EX_NULL_VALUE_INJECTED);

            this._userRepository = userRepository;
            this._feedService = feedService;
        }

        public async Task<User> AsociateFeedsAsync(int userId, string channel)
        {
            var user = _userRepository.FindUserById(userId);

            if (user == null)
            {
                throw new KeyNotFoundException(Constants.EX_USER_NOT_FOUND);
            }

            var feed = await _feedService.ProcessFeedUriAsync(channel);
            user.AssociateFeeds(new List<Feed>{ feed });
            _userRepository.Save(user);

            return user;
        }

        public List<User> ListUsers()
        {
            return _userRepository.ListUsers();
        }
    }
}
