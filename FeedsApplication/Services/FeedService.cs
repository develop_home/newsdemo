﻿using FeedsDomain.Common;
using FeedsDomain.Entities;
using FeedsDomain.Services;
using FeedsInfrastructure.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FeedsApplication.Services
{
    public class FeedService : IFeedService
    {
        private readonly IFeedRepository _feedRepository;
        private readonly IUserRepository _userRepository;
        private readonly IFeedProcessor _feedConverter;
        private readonly IFeedProcessor _xmlFeedConverter;

        public FeedService(
            IFeedRepository feedRepository,
            IUserRepository userRepository,
            IEnumerable<IFeedProcessor> converters)
        {
            feedRepository = feedRepository ?? throw new ApplicationException(Constants.EX_NULL_VALUE_INJECTED);

            this._feedRepository = feedRepository;
            this._userRepository = userRepository;

            // converters will came in the same order that were defined in the startup
            _feedConverter = converters.ElementAt(0);
            _xmlFeedConverter = converters.ElementAt(1);
        }

        public Feed FindFeedById(int feedId)
        {
            return _feedRepository.FindFeedById(feedId);
        }

        public async Task<IEnumerable<Feed>> GetAllFeedItemsByUserIdAsync(int userId)
        {
            var userFeeds = _feedRepository.GetFeedsByUserId(userId);
            var tasks = new List<Task<Feed>>[userFeeds.Count()];
            var results = new List<Feed>();

            foreach (var userFeed in userFeeds) 
            {
                var taskResult = await GetProcessor(userFeed.Link)
                                        .ProcessAsync(userFeed.Link);
                results.Add(taskResult);
            }

            return results;
        }

        public IEnumerable<Feed> GetFeedsByUserId(int userId)
        {
            return _feedRepository.GetFeedsByUserId(userId);
        }

        public async Task<Feed> ProcessFeedUriAsync(string feedUri)
        {
            return await GetProcessor(feedUri).ProcessAsync(feedUri);
        }

        private IFeedProcessor GetProcessor(string feedUri)
        {
            return feedUri.Contains(".xml") ? _xmlFeedConverter : _feedConverter;
        } 
    }
}
