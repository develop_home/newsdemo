﻿using FeedsDomain.Common;
using FeedsDomain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace FeedsApplication.Utils
{
    public class FeedProcessor : IFeedProcessor
    {
        private readonly HttpClient _httpClient;
        private readonly IXmlToFeedConverter _xmlToFeedConverter;

        public FeedProcessor(HttpClient httpClient, IXmlToFeedConverter xmlToFeedConverter)
        {
            this._httpClient = httpClient;
            this._xmlToFeedConverter = xmlToFeedConverter;
        }

        public async Task<Feed> ProcessAsync(string requestUri)
        {
            if (!Uri.IsWellFormedUriString(requestUri, UriKind.Absolute))
            {
                throw new ArgumentException(Constants.EX_INVALID_URL_VALIDATION, "url");
            }

            HttpResponseMessage httpResponseMessage = await _httpClient.GetAsync(requestUri);
            httpResponseMessage.EnsureSuccessStatusCode();

            var stringDocument = await httpResponseMessage.Content.ReadAsStringAsync();

            return await ProcessResponse(stringDocument, requestUri);
        }

        public async Task<Feed> ProcessResponse(string value, string requestUri)
        {
            // This code can be interpreted as UnhandledException but it's fine
            // If occurs, be sure to have "Just My Code" disabled 
            // or run it with Ctrl + F5
            try
            {
                var xmlDoc = await Task.Run(() =>
                {
                    XmlDocument document = new XmlDocument();
                    document.LoadXml(value);
                    return document;
                });

                return _xmlToFeedConverter.GetChannelItems(xmlDoc, requestUri);
            }
            catch (XmlException)
            {
                throw new InvalidOperationException(Constants.EX_ERROR_PROCESSING_DOCUMENT);
            }

        }
    }
}
