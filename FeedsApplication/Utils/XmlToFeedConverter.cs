﻿using FeedsDomain.Common;
using FeedsDomain.Entities;
using System.Collections.Generic;
using System.Xml;
using System.Linq;
using System;

namespace FeedsApplication.Utils
{
    public class XmlToFeedConverter : IXmlToFeedConverter
    {
        public Feed GetChannelItems(XmlDocument xmlDoc, string feedUri)
        {
            var chanelItems = new List<Item>();

            var returnElement = LoadFeedFromDocument(xmlDoc, feedUri);

            if (DocumentHasItems(xmlDoc)) {
                var items = LoadItemsFromDocument(xmlDoc).ToList();
                returnElement.AddFeedItems(items);
            }

            return returnElement;
        }

        private Feed LoadFeedFromDocument(XmlDocument xmlDoc, string feedUri)
        {
            string guid = xmlDoc.SelectSingleNode("//rss/channel/guid")?.InnerText;
            string title = xmlDoc.SelectSingleNode("//rss/channel/title")?.InnerText;
            string link = feedUri;
            string description = xmlDoc.SelectSingleNode("//rss/channel/description")?.InnerText;

            guid = guid ?? Guid.NewGuid().ToString();

            return new Feed(guid, title, link, description);
        }

        private bool DocumentHasItems(XmlDocument xmlDoc)
        {
            return (xmlDoc.SelectSingleNode("//rss/channel/item") != null);
        }

        private IEnumerable<Item> LoadItemsFromDocument(XmlDocument xmlDoc)
        {
            foreach (XmlNode node in xmlDoc.SelectNodes("//rss/channel/item"))
            {
                yield return LoadItemFromXmlNode(node);
            }
        }

        private Item LoadItemFromXmlNode(XmlNode xmlItemNode)
        {
            string guid = xmlItemNode["guid"].InnerText;
            string title = xmlItemNode["title"].InnerText;
            string link = xmlItemNode["link"].InnerText;
            string description = xmlItemNode["description"].InnerText;
            string datePub = xmlItemNode["pubDate"].InnerText;

            guid = guid ?? Guid.NewGuid().ToString();
            var date = string.IsNullOrWhiteSpace(datePub) ? DateTime.Parse(datePub) : DateTime.Now;

            return new Item(guid, title, link, description, date);
        }
    }
}
