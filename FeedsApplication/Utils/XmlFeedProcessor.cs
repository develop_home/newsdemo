﻿using FeedsDomain.Common;
using FeedsDomain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Xml;

namespace FeedsApplication.Utils
{
    public class XmlFeedProcessor : IFeedProcessor
    {
        private readonly IXmlToFeedConverter _xmlToFeedConverter;

        public XmlFeedProcessor(IXmlToFeedConverter xmlToFeedConverter)
        {
            this._xmlToFeedConverter = xmlToFeedConverter;
        }

        public async Task<Feed> ProcessAsync(string requestUri)
        {
            if (!Uri.IsWellFormedUriString(requestUri, UriKind.Absolute))
            {
                throw new ArgumentException(Constants.EX_INVALID_URL_VALIDATION, "url");
            }

            return await ProcessResponse(requestUri, requestUri);
        }

        public async Task<Feed> ProcessResponse(string value, string requestUri)
        {
            var returnElement = new Feed();
            var chanelItems = new List<Item>();

            // This code can be interpreted as UnhandledException but it's fine
            // If occurs, be sure to have "Just My Code" disabled 
            // or run it with Ctrl + F5
            try
            {                    
                var xmlDoc = await Task.Run(() => {
                    var document = new XmlDocument();
                    document.Load(value);
                    return document;
                });

                return _xmlToFeedConverter.GetChannelItems(xmlDoc, requestUri);
            }
            catch (XmlException ex)
            {
                throw new InvalidOperationException(Constants.EX_ERROR_PROCESSING_DOCUMENT, ex);
            }
        }
    }
}
