﻿using System;
using System.Runtime.Serialization;

namespace FeedsDomain.Exceptions
{
    [Serializable]
    public class FeedsValidationException : Exception
    {
        public FeedsValidationException()
        {
        }

        public FeedsValidationException(string message) : base(message)
        {
        }

        public FeedsValidationException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected FeedsValidationException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
