﻿using FeedsDomain.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FeedsDomain.Services
{
    public interface IUserService
    {
        Task<User> AsociateFeedsAsync(int userId, string channel);
        
        List<User> ListUsers();
    }
}
