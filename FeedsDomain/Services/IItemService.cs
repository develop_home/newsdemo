﻿using FeedsDomain.Entities;
using System.Collections.Generic;

namespace FeedsDomain.Services
{
    public interface IItemService
    {
        IEnumerable<Item> GetItemsByFeedId(int feedId);
    }
}
