﻿using FeedsDomain.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FeedsDomain.Services
{
    public interface IFeedService
    {
        Feed FindFeedById(int feedId);

        Task<Feed> ProcessFeedUriAsync(string feedUri);

        Task<IEnumerable<Feed>> GetAllFeedItemsByUserIdAsync(int userId);

        IEnumerable<Feed> GetFeedsByUserId(int userId);
    }
}
