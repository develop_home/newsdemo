﻿using FeedsDomain.Common;
using FeedsDomain.Entities;
using System.Collections.Generic;

namespace FeedsDomain.Repositories
{
    public interface IRepository<T> where T : Entity
    {
        void Save(T entityToSave);
        void SaveBatch(IEnumerable<T> entitiesToSave);
    }
}
