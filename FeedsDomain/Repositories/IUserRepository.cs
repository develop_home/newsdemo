﻿using FeedsDomain.Entities;
using FeedsDomain.Repositories;
using System.Collections.Generic;

namespace FeedsInfrastructure.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        User FindUserById(int userId);

        User FindUser(string userName);

        List<User> ListUsers();
    }
}
