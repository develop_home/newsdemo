﻿using FeedsDomain.Entities;
using FeedsDomain.Repositories;
using System;
using System.Collections.Generic;

namespace FeedsInfrastructure.Repositories
{
    public interface IFeedRepository : IRepository<Feed>
    {
        IEnumerable<Feed> GetFeedsByUserId(int userId);

        Feed FindFeedById(int feedId);
    }
}
