﻿using FeedsDomain.Comparers;
using System.Collections.Generic;
using System.Linq;

namespace FeedsDomain.Entities
{
    public class Feed : Entity
    {
        public Feed() { }

        public Feed(string guid, string title, string link, string description)
        {
            this.Title = title;
            this.Link = link;
            this.Description = description;
            this.Guid = guid;
        }

        public Feed(string guid, string title, string link, string description, User user)
                        : this(guid, title, link, description)
        {
            this.User = user;
        }

        // in some cases the providers put an url instead of a guid value, 
        // string is more representative
        public string Guid { get; private set; }

        public string Title { get; private set; }

        public string Link { get; private set; }

        public string Description { get; private set; }

        public User User { get; private set; }

        private readonly List<Item> _items = new List<Item>();

        public IEnumerable<Item> Items => _items.AsReadOnly();

        public void AddFeedItems(List<Item> items) {

            //var filtered = items.Except(_items, new ItemComparer());

            _items.AddRange(items); 
        }

        public void SetUser(User user)
        {
            this.User = user; 
        }
    }
}
