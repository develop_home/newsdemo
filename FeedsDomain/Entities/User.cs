﻿using FeedsDomain.Common;
using FeedsDomain.Comparers;
using System.Collections.Generic;
using System.Linq;

namespace FeedsDomain.Entities
{
    public class User : Entity, IRoot
    {
        public User() {
        }

        public User(
            string username,
            string firstName,
            string lastName) : this()
        {
            this.Username = username;
            this.FirstName = firstName;
            this.LastName = lastName;
        }

        public string Username { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }
        
        private readonly List<Feed> _feeds = new List<Feed>();

        public IEnumerable<Feed> Feeds()
        {
            return _feeds.AsReadOnly();
        }

        public void AssociateFeeds(List<Feed> userFeeds)
        {
            var filtered = userFeeds.Except(_feeds, new FeedComparer());

            filtered.ToList().ForEach(feed => feed.SetUser(this));

            _feeds.AddRange(filtered);
        }
    }
}
