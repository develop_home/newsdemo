﻿using System;

namespace FeedsDomain.Entities
{
    public class Item : Entity
    {
        public Item() {}

        public Item(string guid, string title, string link, string description, DateTime pubDate)
        {
            this.Guid = guid;
            this.Title = title;
            this.Link = link;
            this.Description = description;
            this.PubDate = pubDate;
        }

        public Item(string guid, string title, string link, string description, DateTime pubDate, Feed feed) : 
            this(guid, title, link, description, pubDate)
        {
            this.Feed = feed;
        }

        // in some cases the providers put an url instead of a guid value, 
        // string is more representative
        public string Guid { get; private set; }
        
        public string Title { get; private set; }

        public string Link { get; private set; }

        public string Description { get; private set; }

        public DateTime PubDate { get; private set; }

        public Feed Feed { get; private set; }
    }
}
