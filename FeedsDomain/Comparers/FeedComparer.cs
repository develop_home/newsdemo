﻿using FeedsDomain.Entities;
using System.Collections.Generic;

namespace FeedsDomain.Comparers
{
    public class FeedComparer : IEqualityComparer<Feed>
    {
        public bool Equals(Feed x, Feed y)
        {
            return x.Guid == y.Guid;
        }

        public int GetHashCode(Feed obj)
        {
            return obj.Guid.GetHashCode();
        }
    }
}
