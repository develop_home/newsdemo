﻿using FeedsDomain.Entities;
using System.Collections.Generic;

namespace FeedsDomain.Comparers
{
    public class ItemComparer : IEqualityComparer<Item>
    {
        public bool Equals(Item x, Item y)
        {
            return x.Guid == y.Guid;
        }

        public int GetHashCode(Item obj)
        {
            return obj.Guid.GetHashCode();
        }
    }
}
