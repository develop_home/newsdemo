﻿using FeedsDomain.Entities;
using System.Threading.Tasks;

namespace FeedsDomain.Common
{
    public interface IFeedProcessor
    {
        Task<Feed> ProcessAsync(string requestUri);
    }
}
