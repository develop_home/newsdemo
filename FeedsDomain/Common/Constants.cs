﻿namespace FeedsDomain.Common
{
    public class Constants
    {
        public const string EX_EMPTY_USERNAME_VALIDATION = "The username cannot be empty";
        public const string EX_INVALID_FEEDID_VALIDATION = "The feed is invalid";
        public const string EX_INVALID_URL_VALIDATION = "The chanel URL is not valid";
        public const string EX_NULL_VALUE_INJECTED = "The injected value cannot be null";
        public const string EX_USER_NOT_FOUND = "The id does not correspont to any user";
        public const string EX_ERROR_PROCESSING_DOCUMENT = "There was an error processing the document";        
    }
}
