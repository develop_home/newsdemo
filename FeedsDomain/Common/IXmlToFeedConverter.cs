﻿using FeedsDomain.Entities;
using System.Xml;

namespace FeedsDomain.Common
{
    public interface IXmlToFeedConverter
    {
        Feed GetChannelItems(XmlDocument xmlDoc, string feedUri);
    }
}
